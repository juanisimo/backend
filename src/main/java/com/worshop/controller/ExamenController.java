package com.worshop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

//import javax.validation.Valid;

//import java.util.ArrayList;
//import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.Examen;
import com.worshop.service.IExamenService;



@RestController
@RequestMapping("/Examen")

public class ExamenController {
	


	 IExamenService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Examen>> listar(){
		List<Examen> especialistas=new ArrayList<>();
		especialistas=service.getAll();
		return new ResponseEntity<List<Examen>>(especialistas,HttpStatus.OK);
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Examen registrar(@RequestBody Examen v) {
		return service.persist(v);
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen>listarId(@PathVariable("id") Integer id){
		Examen especialista=service.findBYId(id);
		return new ResponseEntity<Examen>(especialista,HttpStatus.OK);
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @RequestBody Examen especialidad){
		service.merge(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping
	public void eliminar(@PathVariable Integer id) {
		Examen espec=service.findBYId(id);
		//if(espec==null) {
	//		throw new ModeNotFoundException("ID"+id);
	//	}else {
			service.delete(id);
		//}
	}
}


