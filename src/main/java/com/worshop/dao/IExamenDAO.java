package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;

import com.worshop.models.Examen;



public interface IExamenDAO extends JpaRepository<Examen,Integer> {

}
