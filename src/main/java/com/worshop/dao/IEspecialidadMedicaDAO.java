package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.worshop.models.EspecialidadMedica;

@Repository

public interface IEspecialidadMedicaDAO extends JpaRepository<EspecialidadMedica,Integer> {

}
