package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.worshop.models.especialista;

public interface IEspecialistaDAO extends JpaRepository<especialista,Integer> {

}
