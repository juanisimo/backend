package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.worshop.models.Paciente;



public interface IPacienteDAO extends JpaRepository<Paciente,Integer> {

}
