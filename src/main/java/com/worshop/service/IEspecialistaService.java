package com.worshop.service;

import java.util.List;

import com.worshop.models.especialista;



public interface IEspecialistaService {
	especialista persist(especialista e);
	List<especialista> getAll();
	especialista findBYId(Integer id);
	especialista merge(especialista e);
	void delete(Integer id);

}
