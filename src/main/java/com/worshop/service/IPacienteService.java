package com.worshop.service;

import java.util.List;

import com.worshop.models.Paciente;



public interface IPacienteService {
	
	Paciente persist(Paciente e);
	List<Paciente> getAll();
	Paciente findBYId(Integer id);
	Paciente merge(Paciente e);
	void delete(Integer id);

}
