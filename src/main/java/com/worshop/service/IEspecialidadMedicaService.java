package com.worshop.service;

import java.util.List;

import com.worshop.models.EspecialidadMedica;

public interface IEspecialidadMedicaService {
	EspecialidadMedica persist(EspecialidadMedica e);
	List<EspecialidadMedica> getAll();
	EspecialidadMedica findBYId(Integer id);
	EspecialidadMedica merge(EspecialidadMedica e);
	void delete(Integer id);

}
