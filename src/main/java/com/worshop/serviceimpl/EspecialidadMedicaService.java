package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.IEspecialidadMedicaDAO;
import com.worshop.models.EspecialidadMedica;
import com.worshop.service.IEspecialidadMedicaService;
@Service

public class EspecialidadMedicaService implements IEspecialidadMedicaService {
	@Autowired
	IEspecialidadMedicaDAO service;

	@Override
	public EspecialidadMedica persist(EspecialidadMedica e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<EspecialidadMedica> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public EspecialidadMedica findBYId(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public EspecialidadMedica merge(EspecialidadMedica e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
		
	}

}
